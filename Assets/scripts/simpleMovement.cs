using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class simpleMovement : MonoBehaviour
{
    [SerializeField] float speed;
    private Rigidbody myrb;
    // Start is called before the first frame update
    void Start()
    {
       myrb = GetComponent<Rigidbody>(); 
    }

    // Update is called once per frame
    void Update()
    {
        myrb.velocity = new Vector3 (speed * Input.GetAxis("Horizontal"),0,0);
    }
}
