 using UnityEngine;
 using System.Collections;
 using System;
 using UnityEngine.Events;
 using UnityEngine.EventSystems;
 using UnityEngine.UI;

public class TriggerFunction : MonoBehaviour
{
    [Serializable]
    public class TriggerEnter : UnityEvent { }
    public class TriggerExit : UnityEvent { }

    [SerializeField]
    private TriggerEnter onEnter = new TriggerEnter();
    public TriggerEnter onEnterRun {get {return onEnter;} set { onEnter = value;}}

    [SerializeField]
    private TriggerEnter onExit = new TriggerEnter();
    public TriggerEnter onExitRun {get {return onExit;} set { onExit = value;}}

    [SerializeField] 


    void OnTriggerEnter(){
        onEnterRun.Invoke();
    }   
    void OnTriggerExit(){
        onExitRun.Invoke();
    }
}

